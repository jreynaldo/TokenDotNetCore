﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebAppPaises.Model;

namespace WebAppPaises.Controllers
{
    [Produces("application/json")]
    [Route("api/Pais/{PaisId}/Provincia")]
    public class ProvinciaController : Controller
    {
        private readonly ApplicationDbContext _context;

        public ProvinciaController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/Provincia
        [HttpGet]
        public IEnumerable<Provincia> GetProvincias(int PaisId)
        {
            return _context.Provincias.Where(x =>x.PaisId == PaisId).ToList();
        }

        // GET: api/Provincia/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetProvincia([FromRoute] int PaisId,int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var provincia = await _context.Provincias.Where(x => x.PaisId == PaisId).SingleOrDefaultAsync(m => m.Id == id);

            if (provincia == null)
            {
                return NotFound();
            }

            return Ok(provincia);
        }

        // PUT: api/Provincia/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutProvincia([FromRoute] int PaisId, int id, [FromBody] Provincia provincia)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != provincia.Id && PaisId == provincia.PaisId)
            {
                return BadRequest();
            }

            _context.Entry(provincia).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProvinciaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Provincia
        [HttpPost]
        public async Task<IActionResult> PostProvincia([FromBody] Provincia provincia, int PaisId)
        {
            provincia.PaisId = PaisId;
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Provincias.Add(provincia);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetProvincia", new { id = provincia.Id }, provincia);
        }

        // DELETE: api/Provincia/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteProvincia([FromRoute] int PaisId, int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var provincia = await _context.Provincias.SingleOrDefaultAsync(m => m.Id == id &&  m.PaisId ==PaisId);
            if (provincia == null)
            {
                return NotFound();
            }

            _context.Provincias.Remove(provincia);
            await _context.SaveChangesAsync();

            return Ok(provincia);
        }

        private bool ProvinciaExists(int id)
        {
            return _context.Provincias.Any(e => e.Id == id);
        }
    }
}